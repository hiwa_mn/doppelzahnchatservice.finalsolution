﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.ApplicationServices
{
    public interface IGetAllNotifications : IApplicationService
    {
        List<Notification> Execute();
    }
}
