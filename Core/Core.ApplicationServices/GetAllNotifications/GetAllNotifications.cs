﻿using Core.Contracts;
using Core.Entities;
using Core.Entities.Functions;
using Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Core.ApplicationServices
{
    public class GetAllNotifications : IGetAllNotifications
    {
        private readonly IUnitOfWork unit;
        
        public GetAllNotifications(IUnitOfWork unit)
        {
            this.unit = unit;
            
        }
        public List<Notification> Execute()
        {
            return unit.Notifications.GetAll();
        }

    }
}
