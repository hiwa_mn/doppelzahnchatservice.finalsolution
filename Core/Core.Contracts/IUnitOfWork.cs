﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Contracts
{
    public interface IUnitOfWork
    {        
        INotificationRepository Notifications { get;}

        void Complete();
    }
}
