#pragma checksum "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1472df04527c705f8165ffa32ac9ce6212bff6b9"
// <auto-generated/>
#pragma warning disable 1591
namespace ChatEndPoint.Server.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Entities.ChatElements;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Utility.Tools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Controllers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Contracts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Infrastructure.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.ApplicationServices;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using ChatEndPoint.Server.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using System.Text.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using Microsoft.AspNetCore.SignalR.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using ChatEndPoint.Server.Hubs;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using ChatEndPoint.Server.Hubs.HubEvent;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using Core.Entities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using WSDT.EmojiFilePicker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
using Markdig;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/")]
    public partial class Chat : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.AddMarkupContent(0, @"<style>
    .resizedImg {
        height: 200px;
        background-color: #ccc;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        /* border-radius */
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        /* box-shadow */
        -webkit-box-shadow: rgba(0,0,0,0.8) 0px 0 3px;
        -moz-box-shadow: rgba(0,0,0,0.8) 0 0 3px;
        box-shadow: rgba(0,0,0,0.8) 0 0 3px;
        cursor: pointer;
        transition: opacity 1s;
    }

        .resizedImg:hover {
            opacity: 0.5;
        }
</style>


");
            __builder.OpenElement(1, "div");
            __builder.AddAttribute(2, "class", "card");
            __builder.AddAttribute(3, "style", "margin:15px;");
            __builder.AddMarkupContent(4, "\r\n    ");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "card-header");
            __builder.AddMarkupContent(7, "\r\n        ");
            __builder.AddMarkupContent(8, "<h5>Doppelzahn-Chat</h5>\r\n        ");
            __builder.OpenElement(9, "h6");
            __builder.AddAttribute(10, "class", "card-subtitle mb-2 text-muted");
            __builder.AddMarkupContent(11, "\r\n            ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Authorization.AuthorizeView>(12);
            __builder.AddAttribute(13, "Authorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.AddContent(14, "You are logged in");
            }
            ));
            __builder.AddAttribute(15, "NotAuthorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.AddContent(16, "Log in to access all features");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(17, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(18, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(19, "\r\n\r\n    ");
            __builder.OpenElement(20, "ul");
            __builder.AddAttribute(21, "id", "messagesList");
            __builder.AddAttribute(22, "class", "list-group list-group-flush");
            __builder.AddMarkupContent(23, "\r\n");
#nullable restore
#line 53 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
         foreach (var UserMessage in chatService.AllMessages)
        {

#line default
#line hidden
#nullable disable
            __builder.AddContent(24, "            ");
            __builder.OpenElement(25, "li");
            __builder.AddAttribute(26, "class", "list-group-item");
            __builder.AddAttribute(27, "style", 
#nullable restore
#line 55 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                 UserMessage.User.Equals(ChatSystemMsg.SystemUser) ? "color: #ccc;font-style:italic;font-weight:bold;" : ""

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(28, "\r\n                ");
            __builder.OpenElement(29, "strong");
            __builder.AddContent(30, 
#nullable restore
#line 56 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                         UserMessage.User.EMail

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddContent(31, " ");
            __builder.OpenElement(32, "sup");
            __builder.AddContent(33, 
#nullable restore
#line 56 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                               UserMessage.GetTimeDifference()

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(34, "\r\n");
#nullable restore
#line 57 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                 if (UserMessage.User.IsGuest)
                {

#line default
#line hidden
#nullable disable
            __builder.AddContent(35, "                    ");
            __builder.AddMarkupContent(36, "<span class=\"badge badge-pill badge-dark\">Guest</span>\r\n");
#nullable restore
#line 60 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                }

#line default
#line hidden
#nullable disable
            __builder.AddContent(37, "                ");
            __builder.OpenElement(38, "div");
            __builder.AddAttribute(39, "style", "font-size:small;");
            __builder.AddMarkupContent(40, "\r\n");
#nullable restore
#line 62 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                     foreach (var Submsg in UserMessage.Messages)
                    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(41, "                        ");
            __builder.OpenElement(42, "div");
            __builder.AddAttribute(43, "class", "animated lightSpeedIn");
            __builder.AddContent(44, 
#nullable restore
#line 64 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                             (MarkupString)Markdown.ToHtml(Submsg)

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(45, "\r\n");
#nullable restore
#line 65 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                    }

#line default
#line hidden
#nullable disable
            __builder.AddContent(46, "                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(47, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(48, "\r\n");
#nullable restore
#line 68 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
        }

#line default
#line hidden
#nullable disable
            __builder.AddContent(49, "    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(50, "\r\n    ");
            __builder.OpenElement(51, "div");
            __builder.AddAttribute(52, "class", "card-body");
            __builder.AddMarkupContent(53, "\r\n        ");
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Forms.EditForm>(54);
            __builder.AddAttribute(55, "Model", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Object>(
#nullable restore
#line 71 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                          chatService.ChatForm

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(56, "OnValidSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Forms.EditContext>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Forms.EditContext>(this, 
#nullable restore
#line 71 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                                     chatService.Send

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(57, "ChildContent", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Forms.EditContext>)((formContext) => (__builder2) => {
                __builder2.AddMarkupContent(58, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Forms.DataAnnotationsValidator>(59);
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(60, "\r\n            ");
                __builder2.OpenComponent<Microsoft.AspNetCore.Components.Authorization.AuthorizeView>(61);
                __builder2.AddAttribute(62, "NotAuthorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder3) => {
                    __builder3.AddMarkupContent(63, "\r\n                    ");
                    __builder3.OpenElement(64, "div");
                    __builder3.AddAttribute(65, "class", "input-group" + " mb-3" + " " + (
#nullable restore
#line 75 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                   chatService.HasSentMsg ? "animated fadeOut":""

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddMarkupContent(66, "\r\n                        ");
                    __builder3.AddMarkupContent(67, "<div class=\"input-group-prepend\">\r\n                            <span class=\"input-group-text\" id=\"addon-mail\">&#x00040;</span>\r\n                        </div>\r\n                        ");
                    __builder3.OpenElement(68, "input");
                    __builder3.AddAttribute(69, "type", "text");
                    __builder3.AddAttribute(70, "class", "form-control");
                    __builder3.AddAttribute(71, "placeholder", "E-Mail");
                    __builder3.AddAttribute(72, "aria-label", "E-Mail");
                    __builder3.AddAttribute(73, "aria-describedby", "addon-inputmail");
                    __builder3.AddAttribute(74, "value", Microsoft.AspNetCore.Components.BindConverter.FormatValue(
#nullable restore
#line 79 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                                                                                                         chatService.ChatForm.CurrentUser.EMail

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(75, "onchange", Microsoft.AspNetCore.Components.EventCallback.Factory.CreateBinder(this, __value => chatService.ChatForm.CurrentUser.EMail = __value, chatService.ChatForm.CurrentUser.EMail));
                    __builder3.SetUpdatesAttributeName("value");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(76, "\r\n                    ");
                    __builder3.CloseElement();
                    __builder3.AddMarkupContent(77, "\r\n                    ");
                    __Blazor.ChatEndPoint.Server.Pages.Chat.TypeInference.CreateValidationMessage_0(__builder3, 78, 79, 
#nullable restore
#line 81 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                              () => chatService.ChatForm.CurrentUser.EMail

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddMarkupContent(80, "\r\n                ");
                }
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(81, "\r\n\r\n            ");
                __builder2.AddMarkupContent(82, "<small style=\"font-style:italic;font-weight: bold;\">Markdown and Html are supported :-)</small>\r\n            ");
                __builder2.OpenComponent<WSDT.EmojiFilePicker.EmojiInput>(83);
                __builder2.AddAttribute(84, "IsSubmitDisabled", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 86 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                            !chatService.IsConnected()

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(85, "Placeholder", "Message..");
                __builder2.AddAttribute(86, "ShowSubmit", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 86 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                                                             true

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(87, "CustomSymbols", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.List<System.String>>(
#nullable restore
#line 86 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                                                                                   CustomSymbols

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(88, "AddFiles", new System.Action<System.Collections.Generic.List<System.String>>(
#nullable restore
#line 87 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                            AddFiles

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(89, "FileUploadRoute", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 87 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                                                                                         ICommonRoutes.FILE_UPLOAD

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(90, "Message", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 87 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                               chatService.ChatForm.MessageInput

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(91, "MessageChanged", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<System.String>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<System.String>(this, Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.CreateInferredEventCallback(this, __value => chatService.ChatForm.MessageInput = __value, chatService.ChatForm.MessageInput))));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(92, "\r\n            ");
                __Blazor.ChatEndPoint.Server.Pages.Chat.TypeInference.CreateValidationMessage_1(__builder2, 93, 94, 
#nullable restore
#line 88 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
                                      () => chatService.ChatForm.MessageInput

#line default
#line hidden
#nullable disable
                );
                __builder2.AddMarkupContent(95, "\r\n        ");
            }
            ));
            __builder.CloseComponent();
            __builder.AddMarkupContent(96, "\r\n\r\n\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(97, "\r\n    ");
            __builder.AddMarkupContent(98, "<div class=\"card-footer text-muted\">\r\n        <small>More on <a href=\"https://github.com/wsdt\" title=\"Github - WSDT\">Github/WSDT</a>.</small>\r\n    </div>\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 99 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\Chat.razor"
       
    //ChatService chatService = new ChatService();
    [CascadingParameter] Task<AuthenticationState> AuthenticationStateTask { get; set; }

    private List<string> CustomSymbols = new List<string>()
{
        "⺀", "⽥", "⽨", "⽣", "㣕"
    };

    private void AddToMsg(string msg) => chatService.ChatForm.MessageInput += msg;

    public void AddFiles(List<string> imgUris)
    {
        foreach (string imgUri in imgUris) AddToMsg($"<img src='{imgUri}' alt='Image' class='resizedImg' />");

        chatService.Send();
        StateHasChanged();
    }

    protected override async Task OnInitializedAsync()
    {
        chatService.HubConnection = new HubConnectionBuilder()
            .WithUrl(NavigationManager.ToAbsoluteUri(ICommonRoutes.CHAT_HUB))
            .Build();


        chatService.HubConnection.On<string, MsgPriority>(IPublic.SYSTEM_MSG, (message, priority) =>
        {
            var lastMessage = chatService.AllMessages.LastOrDefault();
            if (lastMessage != null && lastMessage.User.Equals(ChatSystemMsg.SystemUser))
            {
                lastMessage.Messages.Add(message);
            }
            else
            {
                chatService.AllMessages.Add(new ChatSystemMsg(new List<string> { message }, priority));
            }
        });

        chatService.HubConnection.On<string, string>(IPublic.RECEIVE_MSG, (user, message) =>
        {
            var chatUser = JsonSerializer.Deserialize<ChatUser>(user);
            var lastMessage = chatService.AllMessages.LastOrDefault();
            if (lastMessage != null && lastMessage.User.Equals(chatUser))
            {
                // If last message has been written by the same user, add it to the last chatMsg-obj
                lastMessage.Messages.Add(message);
            }
            else
            {
                // other user typed msg
                chatService.AllMessages.Add(
                                new ChatMsg(chatUser, new List<string> { message })
                            );
            }

            StateHasChanged();
        });

        await chatService.HubConnection.StartAsync();
        await SetUsernameIfLoggedIn();
        await chatService.Join(); // notify other chat members about new chatter
    }

    public async Task SetUsernameIfLoggedIn()
    {
        var authState = await AuthenticationStateTask;
        var user = authState.User;
        if (user.Identity.IsAuthenticated)
        {
            chatService.ChatForm.CurrentUser.EMail = user.Identity.Name;
        }
        chatService.ChatForm.CurrentUser.IsGuest = !user.Identity.IsAuthenticated;
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ChatEndPoint.Server.Services.IChatService chatService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager NavigationManager { get; set; }
    }
}
namespace __Blazor.ChatEndPoint.Server.Pages.Chat
{
    #line hidden
    internal static class TypeInference
    {
        public static void CreateValidationMessage_0<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
        public static void CreateValidationMessage_1<TValue>(global::Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder, int seq, int __seq0, global::System.Linq.Expressions.Expression<global::System.Func<TValue>> __arg0)
        {
        __builder.OpenComponent<global::Microsoft.AspNetCore.Components.Forms.ValidationMessage<TValue>>(seq);
        __builder.AddAttribute(__seq0, "For", __arg0);
        __builder.CloseComponent();
        }
    }
}
#pragma warning restore 1591
