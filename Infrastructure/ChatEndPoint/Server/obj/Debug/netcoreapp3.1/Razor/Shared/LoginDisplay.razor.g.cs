#pragma checksum "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Shared\LoginDisplay.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "51daf6efca5c6253ec6950be2a7a39ebf560639a"
// <auto-generated/>
#pragma warning disable 1591
namespace ChatEndPoint.Server.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Entities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Entities.ChatElements;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Utility.Tools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Controllers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Contracts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Infrastructure.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.ApplicationServices;

#line default
#line hidden
#nullable disable
    public partial class LoginDisplay : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenComponent<Microsoft.AspNetCore.Components.Authorization.AuthorizeView>(0);
            __builder.AddAttribute(1, "Authorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(2, "\r\n        ");
                __builder2.OpenElement(3, "a");
                __builder2.AddAttribute(4, "href", "Identity/Account/Manage");
                __builder2.AddContent(5, "Hello, ");
                __builder2.AddContent(6, 
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Shared\LoginDisplay.razor"
                                                  context.User.Identity.Name

#line default
#line hidden
#nullable disable
                );
                __builder2.AddContent(7, "!");
                __builder2.CloseElement();
                __builder2.AddMarkupContent(8, "\r\n        ");
                __builder2.AddMarkupContent(9, "<form method=\"post\" action=\"Identity/Account/LogOut\">\r\n            <button type=\"submit\" class=\"nav-link btn btn-link\">Log out</button>\r\n        </form>\r\n    ");
            }
            ));
            __builder.AddAttribute(10, "NotAuthorized", (Microsoft.AspNetCore.Components.RenderFragment<Microsoft.AspNetCore.Components.Authorization.AuthenticationState>)((context) => (__builder2) => {
                __builder2.AddMarkupContent(11, "\r\n        ");
                __builder2.AddMarkupContent(12, "<a href=\"Identity/Account/Register\">Register</a>\r\n        ");
                __builder2.AddMarkupContent(13, "<a href=\"Identity/Account/Login\">Log in</a>\r\n    ");
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
    }
}
#pragma warning restore 1591
