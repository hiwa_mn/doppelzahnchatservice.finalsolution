#pragma checksum "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\FetchData.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "54412d32921dce3d808e8afbf5a2b01b25f69305"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace ChatEndPoint.Server.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Entities;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Entities.ChatElements;

#line default
#line hidden
#nullable disable
#nullable restore
#line 13 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Utility.Tools;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using ChatEndPoint.Server.Controllers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.Contracts;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Infrastructure.Data;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\_Imports.razor"
using Core.ApplicationServices;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\FetchData.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\FetchData.razor"
           [Authorize]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/fetchdata")]
    public partial class FetchData : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 38 "C:\DoppelzahnChatService\Infrastructure\ChatEndPoint\Server\Pages\FetchData.razor"
       
    private List<Notification> notes;

    protected override async Task OnInitializedAsync()
    {
        try
        {
            notes = getAllNotifications.Execute();

        }
        catch (Exception exception)
        {
            Console.WriteLine(exception.ToString());
        }
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IGetAllNotifications getAllNotifications { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
