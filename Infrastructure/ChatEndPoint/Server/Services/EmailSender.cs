﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Utility.Tools.Notification;
using Utility.Tools.SMS.Rahyab;

namespace ChatEndPoint.Server.Services
{
    public class EmailSender : IEmailSender
    {
        public AuthMessageSenderOptions Options { get; }
        private const string DEFAULT_NOREPLY_EMAIL = "kevin.riedl.privat@gmail.com"; // used in case no env var set
        private readonly INotification notification;

        public EmailSender(IOptions<AuthMessageSenderOptions> optionsAccessor,INotification notification)
        {
            this.Options = optionsAccessor.Value;
            this.notification = notification;
        }

        //public Task Execute(string apiKey, string user, string subject, string htmlMessage, string email)
        //{
        //    var client = new SendGridClient(apiKey);


        //    var from = new EmailAddress(
        //        Environment.GetEnvironmentVariable("NOREPLY_EMAIL") ?? DEFAULT_NOREPLY_EMAIL,
        //        user);
        //    var msg = MailHelper.CreateSingleEmail(from, new EmailAddress(email), subject, htmlMessage, htmlMessage);
        //    return client.SendEmailAsync(msg);
        //}

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            await notification.SendAsync(htmlMessage,email);
        }
    }
}
