using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using ChatEndPoint.Server.Services;
using Microsoft.AspNetCore.Identity.UI.Services;
using ChatEndPoint.Server.Hubs;
using ChatEndPoint.Server.Areas.Identity;
using Utility.Tools;
using Infrastructure.Data;
using Core.Entities;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Infrastructure.Data.Services;
using Core.Contracts;
using EmojiPicker;
using System.Net.Http;
using System;
using Utility.Tools.Notification;
using Utility.Tools.SMS.Rahyab;

namespace ChatEndPoint.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IHostEnvironment HostEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRepositories();
            services.AddApplicationServices();
            services.AddScoped<IChatService, ChatService>();
            services.AddScoped<INotification, EmailService>();
            services.AddScoped<IContext, MainContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<MainContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<User>()
                .AddEntityFrameworkStores<MainContext>();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, RevalidatingIdentityAuthenticationStateProvider<User>>();
           
            // SignalR for chat feature
            services.AddSignalR();
            services.AddResponseCompression(opts =>
            {
                opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                    new[] { "application/octet-stream" });
            });
            services.AddScoped<IChatService, ChatService>();

            // E-Mail (e.g. account confirmation)
            services.AddTransient<IEmailSender, EmailSender>();
            

            //services.Configure<AuthMessageSenderOptions>(Configuration);

            // Emoji Picker Library
            services.AddEmojiPicker();
            

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
                endpoints.MapBlazorHub();
                endpoints.MapHub<ChatHub>(ICommonRoutes.CHAT_HUB);
                endpoints.MapFallbackToPage("/_Host");
            });

        }
    }
}
