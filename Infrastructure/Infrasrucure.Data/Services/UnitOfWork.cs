﻿using Core.Contracts;
using Infrastructure.Data;
using Infrastructure.Data.Services;

namespace Infrastructure.Data.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(MainContext ctx)
        {
            this.ctx = ctx;
        }
        public MainContext ctx { get; set; }

        private INotificationRepository _Notifications;
        public INotificationRepository Notifications
        {
            get
            {
                if (this._Notifications == null)
                {
                    this._Notifications = new NotificationRepository(ctx);
                }
                return this._Notifications;
            }
        }

        public void Complete()
        {
            ctx.SaveChanges();
        }
    }
}
