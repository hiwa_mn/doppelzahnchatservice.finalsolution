﻿using Core.Contracts;
using Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Data.Services
{
    public class NotificationRepository : Repository<Notification>, INotificationRepository
    {
        private readonly IContext ctx;

        public NotificationRepository(IContext ctx) : base(ctx as DbContext)
        {
            this.ctx = ctx;
        }

        
    }
}
