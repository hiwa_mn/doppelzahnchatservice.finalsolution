﻿using Core.ApplicationServices;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace Infrastructure.Data
{
    public static class Extension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            var applicationServiceType = typeof(IApplicationService).Assembly;
            var AllApplicationServices = applicationServiceType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.FullName.Contains("ApplicationService")).ToList();
            foreach (var type in AllApplicationServices)
            {
                Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }
        public static void AddRepositories(this IServiceCollection services)
        {
            var repositpryType = typeof(Repository<>).Assembly;
            var AllRepositories = repositpryType.ExportedTypes
               .Where(x => x.IsClass && x.IsPublic && x.Name.Contains("Repository") && !x.Name.StartsWith("Repository")).ToList();
            foreach (var type in AllRepositories)
            {
                //Console.WriteLine(type.Name);
                services.AddScoped(type.GetInterface($"I{type.Name}"), type);
            }
        }
        public static void ConfigureServices(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            services.AddApplicationServices();
            services.AddRepositories();
        }
        public static void Seed(this ModelBuilder builder)
        {
            builder.Entity<User>().HasData(
                new User
                {
                    Id = "7d444f6e-b4df-4214-84de-aee141ea5b85",
                    Email = "hiwa_mn@yahoo.com",
                    EmailConfirmed = true,
                    NormalizedEmail = "hiwa_mn@yhaoo.com",
                    NormalizedUserName = "hiwa_mn@yhaoo.com",
                    PasswordHash = "AQAAAAEAACcQAAAAEI0AnMvxQH0OU6zDQ+YAQdlzjfIqMkN9ZsUUEdwPYvaUvLHh8f9iZ9+AdNHLWCSeOA==",
                    PhoneNumberConfirmed = true,
                    SecurityStamp = "KEUARTLC2USSHDXX4KZ27NR7BMRZXCTL",
                    UserName = "hiwa_mn@yhaoo.com",
                    ConcurrencyStamp = "c175fe47-7aa9-4cb6-8f53-a0fffe04b691"
                });
            builder.Entity<Notification>().HasData(
                new Notification
                {
                    CreatedAt = 1594972888,//Unix based
                    Description = "Hello",
                    Link = "http://iranfullstack.ir",
                    Title = "My first notification",
                    UserId = Guid.Parse("7d444f6e-b4df-4214-84de-aee141ea5b85"),
                    Id = Guid.Parse("7d444f6e-b4df-4214-84de-aee141ea5b85")
                }); 
            builder.Entity<Notification>().HasData(
                 new Notification
                 {
                     CreatedAt = 1594972899,//Unix based
                    Description = "How are you?",
                     Link = "http://iranfullstack.ir",
                     Title = "Response notification",
                     UserId = Guid.Parse("7d444f6e-b4df-4214-84de-aee141ea5b85"),
                     Id = Guid.Parse("7d444f6e-b4df-4214-84de-aee141ea5b86")
                 });
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {

        }
    }
}
