﻿using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Core.Contracts;
using Utility.Tools.Auth;
using Utility.Tools.Notification;
using Core.ApplicationServices;
using Utility.Tools.SMS.Rahyab;
using System.Numerics;
using System.Xml;
using Utility.Tools.RabbitMq.New;
using Utility.Tools.General;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Utility.Tools;
using Infrastructure.Data;

namespace ConsoleCore
{
    class Program
    {
        [Obsolete]
        private static Microsoft.AspNetCore.Hosting.IHostingEnvironment hosting;

        public static IUnitOfWork Iunit { get; private set; }
        public static INotification Notification { get; private set; }
        public static IEncrypter Encrypter { get; private set; }
        public static IJwtHandler JwtHandler { get; private set; }
        public static IFireBase Firebase { get; private set; }
        public static IConfiguration Configuration { get; set; }

        [Obsolete]
        static void Main()
        {
            Config();            
            Console.ReadKey();
        }

        [Obsolete]
        public static void Config()
        {
            ServiceCollection service = new ServiceCollection();
            var builder = new ConfigurationBuilder().SetBasePath(@"..\Infrastructure\Infrastructure.EndPoint\").AddJsonFile("appsettings.json", optional: true, reloadOnChange: true).AddEnvironmentVariables();
            Configuration = builder.Build();

            service.AddSingleton<IConfiguration>(builder.Build());            
            service.ConfigureServices(Configuration);
            service.AddDbContext<MainContext>(o => o.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            var provider = service.BuildServiceProvider();
            Iunit = provider.GetService<IUnitOfWork>();
            Encrypter = provider.GetService<IEncrypter>();
            Notification = provider.GetService<INotification>();
            JwtHandler = provider.GetService<IJwtHandler>();
            Firebase = provider.GetService<IFireBase>();
            hosting = provider.GetService<Microsoft.AspNetCore.Hosting.IHostingEnvironment>();
        }
        
    }
}
